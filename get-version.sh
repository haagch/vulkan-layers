#!/usr/bin/env bash
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2018-2022 Collabora, Ltd. and the Monado contributors
#
# Wraps versioning.cmake for use from command line
set -euo pipefail

CI_COMMIT_SHA=${CI_COMMIT_SHA:-HEAD}
REPO_DIR=$(cd "$(dirname "$0")" && pwd)

GIT_DESCRIBE=$(git describe --always "${CI_COMMIT_SHA}")
FULL_VER=$(cmake "-DINPUT=$GIT_DESCRIBE" -P "${REPO_DIR}/versioning.cmake" 2>&1)

echo "$FULL_VER"
