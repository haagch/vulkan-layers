#!/bin/sh
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2018-2022 Collabora, Ltd. and the Monado contributors

set -e

(
    cd "$(dirname "$0")"
    ./install-ci-fairy.sh
    python3 -m pip install git-archive-all
)
