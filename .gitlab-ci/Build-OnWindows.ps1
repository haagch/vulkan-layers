# Copyright 2019-2022, Mesa contributors
# Copyright 2022, Collabora, Ltd.
# SPDX-License-Identifier: MIT
# Based on https://gitlab.freedesktop.org/mesa/mesa/-/blob/8396df5ad90aeb6ab2267811aba2187954562f81/.gitlab-ci/windows/mesa_build.ps1

[CmdletBinding()]
param (
    # Should we install the project?
    [Parameter()]
    [switch]
    $Install = $false,

    # Should we package the project?
    [Parameter()]
    [switch]
    $Package = $false,

    # Should we run the test suite?
    [Parameter()]
    [switch]
    $RunTests = $false
)
$ErrorActionPreference = 'Stop'

Get-Date
Write-Host "Compiling"
$sourcedir = (Resolve-Path "$PSScriptRoot/..")
$builddir = Join-Path $sourcedir "build"
$installdir = Join-Path $sourcedir "install"
# $vcpkgdir = "c:\vcpkg"
# $toolchainfile = Join-Path $vcpkgdir "scripts/buildsystems/vcpkg.cmake"

Remove-Item -Recurse -Force $installdir -ErrorAction SilentlyContinue

Write-Host "builddir:$builddir"
Write-Host "installdir:$installdir"
Write-Host "sourcedir:$sourcedir"
# Write-Host "toolchainfile:$toolchainfile"

$installPath = vswhere -version 17 -latest -products * -property installationpath
Write-Host "vswhere.exe installPath: $installPath"
if (!$installPath) {
    throw "Could not find VS2022 using vswhere!"
}


$devshell = vswhere -version 17 -products * -latest -find "**\Microsoft.VisualStudio.DevShell.dll"
if (!$devshell) {
    throw "Could not find VS2022 devshell module using vswhere!"
}
Write-Host "devshell: $devshell"
Import-Module $devshell
Enter-VsDevShell -VsInstallPath $installPath -SkipAutomaticLocation -DevCmdArguments '-arch=x64 -no_logo -host_arch=amd64'

Push-Location $sourcedir

$cmakeArgs = @(
    "-S"
    "."
    "-B"
    "$builddir"
    "-GNinja"
    "-DCMAKE_BUILD_TYPE=RelWithDebInfo"
    # "-DCMAKE_TOOLCHAIN_FILE=$toolchainfile"
    "-DCMAKE_INSTALL_PREFIX=$installdir"
    # "-DVCPKG_MANIFEST_MODE=OFF"
    # "-DX_VCPKG_APPLOCAL_DEPS_INSTALL=ON"
)
cmake @cmakeArgs
if (!$?) {
    throw "cmake generate failed!"
}

Write-Host "Building"
cmake --build $builddir
if (!$?) {
    throw "cmake build failed!"
}

if ($RunTests) {
    Write-Host "Running tests"
    cmake --build $builddir --target test

}

if ($Install) {
    Write-Host "Installing"
    cmake --build $builddir --target install
}


if ($Package) {
    Write-Host "Packaging"
    cmake --build $builddir --target package
}
