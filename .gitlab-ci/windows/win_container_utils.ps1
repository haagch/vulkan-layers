# Copyright 2019-2022, Mesa contributors
# Copyright 2022, Collabora, Ltd.
# SPDX-License-Identifier: MIT
# Based on https://gitlab.freedesktop.org/mesa/mesa/-/blob/8396df5ad90aeb6ab2267811aba2187954562f81/.gitlab-ci/windows/mesa_deps_vs2019.ps1
# and https://gitlab.freedesktop.org/mesa/mesa/-/blob/8396df5ad90aeb6ab2267811aba2187954562f81/.gitlab-ci/windows/mesa_deps_build.ps1

$ErrorActionPreference = 'Stop'

# VS17.x is 2022
$msvc_url = 'https://aka.ms/vs/17/release/vs_buildtools.exe'


$VulkanRTVersion = "1.3.211.0"

$DefaultVcpkgLocation = "C:\vcpkg"

$DefaultBuildToolsInstallPath = "C:\BuildTools"

$BuildToolsInstaller = "c:\vs_buildtools.exe"

function Install-VS2022BuildTools {
    [CmdletBinding()]
    param (
        $InstallPath = $DefaultBuildToolsInstallPath
    )
    # we want more secure TLS 1.2 for most things
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12;

    Write-Host (Get-Date)
    Write-Host "Downloading Visual Studio 2022 build tools"
    Invoke-WebRequest -Uri $msvc_url -OutFile $BuildToolsInstaller  -UseBasicParsing

    Write-Host (Get-Date)
    Write-Host "Installing Visual Studio build tools"
    $vsInstallerArgs = @(
        "--wait"
        "--quiet"
        "--norestart"
        "--nocache"
        "--installPath"
        $InstallPath
        "--add"
        "Microsoft.VisualStudio.Component.VC.CoreBuildTools"
        "--add"
        "Microsoft.VisualStudio.ComponentGroup.NativeDesktop.Core"
        "--add"
        "Microsoft.VisualStudio.Component.Windows11SDK.22000"
        "--add"
        "Component.Microsoft.Windows.CppWinRT"
        "--add"
        "Microsoft.VisualStudio.Component.VC.Tools.x86.x64"
    )
    Start-Process -NoNewWindow -Wait $BuildToolsInstaller -ArgumentList $vsInstallerArgs
    if (!$?) {
        Write-Error "Failed to install Visual Studio tools"
        Exit 1
    }
    Remove-Item $BuildToolsInstaller -Force

    # Create a directory whose absence would otherwise cause an error with Enter-VsDevShell
    New-Item -Path "$InstallPath\Common7\IDE\CommonExtensions\Microsoft\TeamFoundation\Team Explorer" -ItemType Directory -ErrorAction Continue

    Write-Host (Get-Date)
    Write-Host "Done"
}


function Update-TLSCerts {
    Write-Host (Get-Date)
    Write-Host "Updating TLS certificate store"
    $certdir = (New-Item -ItemType Directory -Name "_tlscerts")
    certutil -syncwithWU "$certdir"
    Foreach ($file in (Get-ChildItem -Path "$certdir\*" -Include "*.crt")) {
        Import-Certificate -FilePath $file -CertStoreLocation Cert:\LocalMachine\Root
    }
    Remove-Item -Recurse -Path $certdir
}

function Install-MSVCRedist {
    Write-Host (Get-Date)
    Write-Host "Installing runtime redistributables"
    Invoke-WebRequest -Uri "https://aka.ms/vs/17/release/vc_redist.x64.exe" -OutFile "c:\vcredist_x64.exe"  -UseBasicParsing
    Start-Process -NoNewWindow -Wait "c:\vcredist_x64.exe" -ArgumentList "/install /passive /norestart /log out.txt"
    if (!$?) {
        Write-Error "Failed to install vc_redist"
    }
    Remove-Item "c:\vcredist_x64.exe" -Force
}

function Install-Scoop {
    Write-Host (Get-Date)
    Write-Host "Installing Scoop"
    Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
    Invoke-WebRequest get.scoop.sh -OutFile c:\install.ps1
    c:\install.ps1 -RunAsAdmin
}

function Install-Vcpkg {
    [CmdletBinding()]
    param (
        [Parameter()]
        [string]
        $Location = $DefaultVcpkgLocation
    )
    Write-Host (Get-Date)
    Write-Host "Cloning vcpkg"
    git clone https://github.com/microsoft/vcpkg.git "$Location"

    Write-Host (Get-Date)
    Write-Host "Bootstrapping vcpkg"
    Push-Location "$Location"
    ./bootstrap-vcpkg.bat -DisableMetrics
    Pop-Location
}


function Install-VcpkgPackages {
    [CmdletBinding()]
    param (
        [Parameter()]
        [string]
        $Location = $DefaultVcpkgLocation
    )
    Push-Location "$Location"
    ./vcpkg.exe install $args
    Remove-Item -Recurse -Path downloads -ErrorAction Continue
    Remove-Item -Recurse -Path buildtrees -ErrorAction Continue
    Pop-Location

}

function Install-VulkanRuntime {
    Write-Host (Get-Date)
    Write-Host "Downloading Vulkan runtime components"

    $VulkanInstaller = "C:\VulkanRTInstaller.exe"
    Invoke-WebRequest -Uri "https://sdk.lunarg.com/sdk/download/$VulkanRTVersion/windows/VulkanRT-$VulkanRTVersion-Installer.exe" -OutFile "$VulkanInstaller"

    Write-Host (Get-Date)
    Write-Host "Installing Vulkan runtime components"
    Start-Process -NoNewWindow -Wait "$VulkanInstaller" -ArgumentList "/S"
    if (!$?) {
        Write-Error "Failed to install Vulkan runtime components"
        throw "failure"
    }
    Remove-Item "$VulkanInstaller" -Force
}

