# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2018-2022 Collabora, Ltd. and the Monado contributors

##

############################################
#      GENERATED - DO NOT EDIT            #
# see .gitlab-ci/ci.template, etc instead #
###########################################
##


stages:
  - container_prep
  - build
  - package
  - reprepro
  - pages
  - deploy

##
##
##
##

variables:
  FDO_UPSTREAM_REPO: monado/utilities/vulkan-layers

.templates_sha: &templates_sha 541748b7644201bac3e3b1ffd69f4ae67a678d93
include:
  # Debian
  - project: "freedesktop/ci-templates"
    ref: *templates_sha
    file: "/templates/debian.yml"
  # Ubuntu
  - project: "freedesktop/ci-templates"
    ref: *templates_sha
    file: "/templates/ubuntu.yml"



# Variables for build and usage of Debian bullseye image
.monado.variables.debian:bullseye:
  variables:
    FDO_DISTRIBUTION_VERSION: "bullseye"
    FDO_DISTRIBUTION_TAG: "2022-06-20.0"

# Variables for build and usage of Ubuntu focal image
.monado.variables.ubuntu:focal:
  variables:
    FDO_DISTRIBUTION_VERSION: "20.04"
    FDO_DISTRIBUTION_TAG: "2022-06-20.0"

# Variables for build and usage of Ubuntu jammy image
.monado.variables.ubuntu:jammy:
  variables:
    FDO_DISTRIBUTION_VERSION: "22.04"
    FDO_DISTRIBUTION_TAG: "2022-06-20.0"

# Variables for build and usage of Ubuntu impish image
.monado.variables.ubuntu:impish:
  variables:
    FDO_DISTRIBUTION_VERSION: "21.10"
    FDO_DISTRIBUTION_TAG: "2022-06-20.0"


###
# Container prep jobs
# Make Debian bullseye image
debian:bullseye:container_prep:
  stage: container_prep
  extends:
    - .monado.variables.debian:bullseye
    - .fdo.container-build@debian # from ci-templates
  only:
    changes:
      - .gitlab-ci/**/*
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'build-essential clang-format cmake codespell curl debhelper devscripts doxygen dput-ng gettext-base git graphviz libvulkan-dev ninja-build patch python3 python3-click python3-pip unzip wget'

# Make Ubuntu focal image
ubuntu:focal:container_prep:
  stage: container_prep
  extends:
    - .monado.variables.ubuntu:focal
    - .fdo.container-build@ubuntu # from ci-templates
  only:
    changes:
      - .gitlab-ci/**/*
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'build-essential cmake curl debhelper devscripts dput-ng gettext-base git libvulkan-dev ninja-build patch python3 unzip wget'

# Make Ubuntu jammy image
ubuntu:jammy:container_prep:
  stage: container_prep
  extends:
    - .monado.variables.ubuntu:jammy
    - .fdo.container-build@ubuntu # from ci-templates
  only:
    changes:
      - .gitlab-ci/**/*
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'build-essential cmake curl debhelper devscripts dput-ng gettext-base git libvulkan-dev ninja-build patch python3 python3-pip reprepro unzip wget'
    FDO_DISTRIBUTION_EXEC: 'bash .gitlab-ci/install-python-packages.sh'

# Make Ubuntu impish image
ubuntu:impish:container_prep:
  stage: container_prep
  extends:
    - .monado.variables.ubuntu:impish
    - .fdo.container-build@ubuntu # from ci-templates
  only:
    changes:
      - .gitlab-ci/**/*
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'build-essential cmake curl debhelper devscripts dput-ng gettext-base git libvulkan-dev ninja-build patch python3 unzip wget'

###
# Container usage base jobs

# Base for using Debian bullseye image
.monado.image.debian:bullseye:
  extends:
    - .monado.variables.debian:bullseye
    - .fdo.distribution-image@debian # from ci-templates


# Base for using Ubuntu focal image
.monado.image.ubuntu:focal:
  extends:
    - .monado.variables.ubuntu:focal
    - .fdo.distribution-image@ubuntu # from ci-templates


# Base for using Ubuntu jammy image
.monado.image.ubuntu:jammy:
  extends:
    - .monado.variables.ubuntu:jammy
    - .fdo.distribution-image@ubuntu # from ci-templates


# Base for using Ubuntu impish image
.monado.image.ubuntu:impish:
  extends:
    - .monado.variables.ubuntu:impish
    - .fdo.distribution-image@ubuntu # from ci-templates


# # # 
###
# Windows container-related jobs (prep and usage)

.monado.common_variables.windows:vs2022:
  inherit:
    default: false
  variables:
    MONADO_WIN_BASE_TAG: "20220727.0"
    MONADO_WIN_MAIN_TAG: "20220727.0"
    MONADO_BASE_IMAGE_PATH: "win2022/vs2022_base"
    MONADO_MAIN_IMAGE_PATH: "win2022/vs2022"

.monado.variables.windows:vs2022:
  inherit:
    default: false
  extends:
    - .monado.common_variables.windows:vs2022
  variables:
    MONADO_IMAGE_PATH: "$MONADO_MAIN_IMAGE_PATH"
    FDO_DISTRIBUTION_TAG: "$MONADO_WIN_MAIN_TAG"
    MONADO_IMAGE: "$CI_REGISTRY_IMAGE/$MONADO_IMAGE_PATH:$FDO_DISTRIBUTION_TAG"
    MONADO_UPSTREAM_IMAGE: "$CI_REGISTRY/$FDO_UPSTREAM_REPO/$MONADO_IMAGE_PATH:$FDO_DISTRIBUTION_TAG"

# Shared container-building job
.monado.windows.container_prep:
  inherit:
    default: false
  tags:
    - windows
    - "2022"
    - shell
  variables:
    GIT_STRATEGY: fetch
  only:
    changes:
      - .gitlab-ci/**/*

  stage: container_prep
  script:
    - |
      .gitlab-ci\windows\monado_container.ps1 -RegistryUri "$CI_REGISTRY" -RegistryUsername "$CI_REGISTRY_USER" -UserImage "$MONADO_IMAGE" -UpstreamImage "$MONADO_UPSTREAM_IMAGE" -Dockerfile "$DOCKERFILE" -BaseImage "$MONADO_BASE_IMAGE" -BaseUpstreamImage "$MONADO_UPSTREAM_BASE_IMAGE" -Verbose

# # This container just installs Visual C++ Build Tools.
# win:container_prep:base:
#   extends:
#     - .monado.windows.container_prep
#     - .monado.common_variables.windows:vs2022
#   variables:
#     DOCKERFILE: Dockerfile.vs2022
#     MONADO_IMAGE_PATH: ${MONADO_BASE_IMAGE_PATH}
#     FDO_DISTRIBUTION_TAG: "$MONADO_WIN_BASE_TAG"
#     MONADO_IMAGE: "$CI_REGISTRY_IMAGE/$MONADO_IMAGE_PATH:$FDO_DISTRIBUTION_TAG"
#     MONADO_UPSTREAM_IMAGE: "$CI_REGISTRY/$FDO_UPSTREAM_REPO/$MONADO_IMAGE_PATH:$FDO_DISTRIBUTION_TAG"

# This container adds other deps
win:container_prep:
  extends:
    - .monado.windows.container_prep
    - .monado.variables.windows:vs2022
  variables:
    DOCKERFILE: Dockerfile
    # MONADO_BASE_IMAGE: "$CI_REGISTRY_IMAGE/$MONADO_BASE_IMAGE_PATH:$MONADO_WIN_BASE_TAG"
    # MONADO_UPSTREAM_BASE_IMAGE: "$CI_REGISTRY/$FDO_UPSTREAM_REPO/$MONADO_BASE_IMAGE_PATH:$MONADO_WIN_BASE_TAG"

# Base job to use a Windows build container
.monado.image.windows:
  tags:
    - windows
    - "2022"
    - docker
  extends:
    - .monado.variables.windows:vs2022
  image: $MONADO_IMAGE
##
##
##
##

# check ci-fairy
ci-fairy:
  stage: build
  extends:
    - .monado.image.ubuntu:jammy
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"'
      changes:
        - .gitlab-ci/**/*
  script:
      # Make sure a full ci-fairy run is a no-op (meaning no generated files were modified on their own)
    - make -f .gitlab-ci/ci-scripts.mk clean all

    # If your build fails here, run the preceding `make` line and commit the changes. See .gitlab-ci/README.md for details
    # We only push if the repo is the upstream one because that's the only place our token works.
    - |
      if ! git diff --exit-code; then
        if [ $CI_PROJECT_PATH = $FDO_UPSTREAM_REPO ]; then
          echo "Automatically re-generating and pushing a new commit."
          git add -u
          git config user.name "Monado Project CI"
          git config user.email "ryan.pavlik@collabora.com"
          git commit -m "ci: Regenerate"
          git push "https://token:${PUSH_TOKEN}@${CI_REPOSITORY_URL#*@}" "HEAD:${CI_COMMIT_REF_NAME}"
        else
          echo "Please commit the results of the following: make -f .gitlab-ci/ci-scripts.mk clean all"
          exit 1
        fi
      fi



debian:cmake:
  stage: build
  extends:
    - .monado.image.debian:bullseye

  script:
    - rm -rf build
    - cmake -GNinja -B build -S .
    - ninja -C build

ubuntu:focal:cmake:
  stage: build
  extends:
    - .monado.image.ubuntu:focal

  script:
    - rm -rf build
    - cmake -GNinja -B build -S .
    - ninja -C build

ubuntu:jammy:cmake:
  stage: build
  extends:
    - .monado.image.ubuntu:jammy

  script:
    - rm -rf build
    - cmake -GNinja -B build -S .
    - ninja -C build

ubuntu:impish:cmake:
  stage: build
  extends:
    - .monado.image.ubuntu:impish

  script:
    - rm -rf build
    - cmake -GNinja -B build -S .
    - ninja -C build

# Windows build
windows:
  stage: build
  extends:
    - .monado.image.windows
  script:
    - ./.gitlab-ci/Build-OnWindows.ps1 -Install -Package
  artifacts:
    when: always
    paths:
      - install
      - build/Testing/Temporary
      - build/*.exe
##
##
##
##








# Packaging

source-tarball:
  stage: build
  interruptible: true
  rules:
    # Only the default branch of the "upstream" repo.
    - if: '$CI_PROJECT_PATH == $FDO_UPSTREAM_REPO && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

    # Or, on release tags
    - if: $CI_COMMIT_TAG

    # Build packages on branches starting with "ci" too, just don't publish them.
    - if: '$CI_PROJECT_PATH == $FDO_UPSTREAM_REPO && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH =~ /^ci.*/'

  extends:
    - .monado.image.ubuntu:jammy
  variables:
    GIT_STRATEGY: clone

  script:
    # Prep the source tree
    - git clean -dfx
    - git fetch --tags
    - .gitlab-ci/make-source-tarball.sh
  artifacts:
    untracked: false
    paths:
      - "monado-vulkan-layers*.tar.xz"
    reports:
      dotenv: sourcetarball.env


debian:bullseye:package:
  stage: package
  rules:
    # Only the default branch of the "upstream" repo.
    - if: '$CI_PROJECT_PATH == $FDO_UPSTREAM_REPO && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

    # Or, on release tags
    - if: $CI_COMMIT_TAG

    # Build packages on branches starting with "ci" too, just don't publish them.
    - if: '$CI_PROJECT_PATH == $FDO_UPSTREAM_REPO && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH =~ /^ci.*/'

  interruptible: true
  extends:
    - ".monado.image.debian:bullseye"
  variables:
    GIT_STRATEGY: clone
    DEBFULLNAME: "Monado CI"
    DEBEMAIL: "ryan.pavlik@collabora.com"
    DISTRO: debian
    CODENAME: bullseye
    DEB_VERSION_SUFFIX: bpo11
  needs:
    - source-tarball
  before_script:
    - 'git config --global user.name "Monado CI"'
    - 'git config --global user.email "ryan.pavlik@collabora.com"'
  script:
    # Unpack the source tarball we got
    - tar xJf *.tar.xz
    - .gitlab-ci/build-and-submit-package.sh
  artifacts:
    paths:
      - "incoming/"
    expire_in: 2 days
    expose_as: 'deb packages for bullseye'
    reports:
      dotenv: bullseye.env

ubuntu:focal:package:
  stage: package
  rules:
    # Only the default branch of the "upstream" repo.
    - if: '$CI_PROJECT_PATH == $FDO_UPSTREAM_REPO && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

    # Or, on release tags
    - if: $CI_COMMIT_TAG

    # Build packages on branches starting with "ci" too, just don't publish them.
    - if: '$CI_PROJECT_PATH == $FDO_UPSTREAM_REPO && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH =~ /^ci.*/'

  interruptible: true
  extends:
    - ".monado.image.ubuntu:focal"
  variables:
    GIT_STRATEGY: clone
    DEBFULLNAME: "Monado CI"
    DEBEMAIL: "ryan.pavlik@collabora.com"
    DISTRO: ubuntu
    CODENAME: focal
    DEB_VERSION_SUFFIX: ubuntu2004
  needs:
    - source-tarball
  before_script:
    - 'git config --global user.name "Monado CI"'
    - 'git config --global user.email "ryan.pavlik@collabora.com"'
  script:
    # Unpack the source tarball we got
    - tar xJf *.tar.xz
    - .gitlab-ci/build-and-submit-package.sh
  artifacts:
    paths:
      - "incoming/"
    expire_in: 2 days
    expose_as: 'deb packages for focal'
    reports:
      dotenv: focal.env

ubuntu:jammy:package:
  stage: package
  rules:
    # Only the default branch of the "upstream" repo.
    - if: '$CI_PROJECT_PATH == $FDO_UPSTREAM_REPO && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

    # Or, on release tags
    - if: $CI_COMMIT_TAG

    # Build packages on branches starting with "ci" too, just don't publish them.
    - if: '$CI_PROJECT_PATH == $FDO_UPSTREAM_REPO && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH =~ /^ci.*/'

  interruptible: true
  extends:
    - ".monado.image.ubuntu:jammy"
  variables:
    GIT_STRATEGY: clone
    DEBFULLNAME: "Monado CI"
    DEBEMAIL: "ryan.pavlik@collabora.com"
    DISTRO: ubuntu
    CODENAME: jammy
    DEB_VERSION_SUFFIX: ubuntu2204
  needs:
    - source-tarball
  before_script:
    - 'git config --global user.name "Monado CI"'
    - 'git config --global user.email "ryan.pavlik@collabora.com"'
  script:
    # Unpack the source tarball we got
    - tar xJf *.tar.xz
    - .gitlab-ci/build-and-submit-package.sh
  artifacts:
    paths:
      - "incoming/"
    expire_in: 2 days
    expose_as: 'deb packages for jammy'
    reports:
      dotenv: jammy.env

ubuntu:impish:package:
  stage: package
  rules:
    # Only the default branch of the "upstream" repo.
    - if: '$CI_PROJECT_PATH == $FDO_UPSTREAM_REPO && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

    # Or, on release tags
    - if: $CI_COMMIT_TAG

    # Build packages on branches starting with "ci" too, just don't publish them.
    - if: '$CI_PROJECT_PATH == $FDO_UPSTREAM_REPO && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH =~ /^ci.*/'

  interruptible: true
  extends:
    - ".monado.image.ubuntu:impish"
  variables:
    GIT_STRATEGY: clone
    DEBFULLNAME: "Monado CI"
    DEBEMAIL: "ryan.pavlik@collabora.com"
    DISTRO: ubuntu
    CODENAME: impish
    DEB_VERSION_SUFFIX: ubuntu2110
  needs:
    - source-tarball
  before_script:
    - 'git config --global user.name "Monado CI"'
    - 'git config --global user.email "ryan.pavlik@collabora.com"'
  script:
    # Unpack the source tarball we got
    - tar xJf *.tar.xz
    - .gitlab-ci/build-and-submit-package.sh
  artifacts:
    paths:
      - "incoming/"
    expire_in: 2 days
    expose_as: 'deb packages for impish'
    reports:
      dotenv: impish.env

reprepro:package:
  stage: reprepro
  interruptible: true
  extends:
    - .monado.image.ubuntu:jammy
  rules:
    # Only the default branch of the "upstream" repo.
    - if: '$CI_PROJECT_PATH == $FDO_UPSTREAM_REPO && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

    # Or, on release tags
    - if: $CI_COMMIT_TAG

  needs:
    - debian:bullseye:package
    - ubuntu:focal:package
    - ubuntu:jammy:package
    - ubuntu:impish:package
  script:
    - bash .gitlab-ci/reprepro.sh
  artifacts:
    paths:
      - "repo/"
    expire_in: 2 days

###
# Pages
###
pages:
  stage: deploy
  rules:
    # Only the default branch of the "upstream" repo.
    - if: '$CI_PROJECT_PATH == $FDO_UPSTREAM_REPO && $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

  needs:
    - "reprepro:package"
  script:
    - mkdir -p public
    - mv repo public/apt
  artifacts:
    paths:
      - public